<?php

use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\FontEnd\ContactController;
use App\Http\Controllers\FontEnd\HomeController as FontEndHomeController;
use App\Http\Controllers\FontEnd\ProductSigleController;
use App\Http\Controllers\FontEnd\ShopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[FontEndHomeController::class,'index']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users',[\App\Http\Controllers\UserController::class,'index']);
Route::middleware('auth', 'isAdmin')->group(function () {

    Route::prefix('admin')->group(function () {
        Route::get('', [HomeController::class, 'index'])->name('admin');
        Route::prefix('products')->group(function () {
            Route::get('', [ProductController::class, 'index'])->name('list.products');
            Route::get('create',[ProductController::class,'create'])->name('create.product');
            Route::post('create',[ProductController::class,'store']);
            Route::get('{id}',[ProductController::class,'show']);
            Route::get('/edit/{id}',[ProductController::class,'edit']);
            Route::put('/edit/{id}',[ProductController::class,'update']);
            Route::delete('/delete/{id}',[ProductController::class,'destroy'])->name('delete.product');
            Route::get('/{product}/add-image',[ImageController::class,'create']);
            Route::post('/{product}/add-image',[ImageController::class,'store']);
            Route::get('{product}/images',[ImageController::class,'index'])->name('list.images.product');
            Route::delete('{product}/images/delete/{image}',[ImageController::class,'destroy'])->name('delete.image.product');
        });
        Route::prefix('banners')->group(function (){
           Route::get('',[BannerController::class,'index'])->name('list.banners');
           Route::get('/create',[BannerController::class,'create'])->name('create.banners');
           Route::post('/create',[BannerController::class,'store']);
           Route::get('/edit/{id}',[BannerController::class,'edit']);
           Route::put('/edit/{id}',[BannerController::class,'update']);
           Route::delete('/delete/{id}',[BannerController::class,'destroy'])->name('delete.banner');
           Route::put('/edit-status/{id}',[BannerController::class,'changeStatus'])->name('change.status.banner');
        });
        Route::prefix('images')->group(function (){
            Route::get('',[ImageController::class,'index'])->name('list.images');
        });
        Route::prefix('posts')->group(function(){
           Route::get('',[PostController::class,'index'])->name('list.posts');
           Route::get('create',[PostController::class,'create'])->name('create.posts');
           Route::post('create',[PostController::class,'store']);
        });
        Route::prefix('contact')->group(function(){
            Route::get('',[App\Http\Controllers\Admin\ContactController::class,'index'])->name('list.contacts');
            Route::delete('/delete/{id}',[\App\Http\Controllers\Admin\ContactController::class,'destroy'])->name('contact.delete');
        });
    });

});
Route::get('/contact',[ContactController::class,'index'])->name('font-end.contact');
Route::post('/contact',[ContactController::class,'store']);
Route::get('/shop',[ShopController::class,'index']);
Route::post('/contact',[ContactController::class,'store']);
Route::get('/product/{id}',[ProductSigleController::class,'show']);
