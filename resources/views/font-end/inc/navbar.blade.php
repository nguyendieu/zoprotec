<div class="py-1 bg-primary">
    <div class="container">
    <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
        <div class="col-lg-12 d-block">
            <div class="row d-flex">
                <div class="col-md pr-4 d-flex topper align-items-center">
                    <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
                    <span class="text"> <a class="text" href="tel://0904350968">(+84) 90 4350968</a></span>
                </div>
                <div class="col-md pr-4 d-flex topper align-items-center">
                    <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
                    <span class="text">senngocminh@gmail.com</span>
                </div>
                <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                    <span class="text">Vận chuyển toàn quốc</span>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
<div class="container">
  <a class="navbar-brand" href="/">
      <img src="{{ asset('assets/admin/img/logo-senngocminh.jpg') }}" height="100" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="oi oi-menu"></span> Menu
  </button>

  <div class="collapse navbar-collapse" id="ftco-nav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active"><a href="/" class="nav-link">Trang chủ</a></li>
      <li class="nav-item dropdown">
      <a class="nav-link" href="/shop ">Cửa hàng</a>
      {{-- <div class="dropdown-menu" aria-labelledby="dropdown04">
          <a class="dropdown-item" href="/shop">Shop</a>
          <a class="dropdown-item" href="wishlist.html">Wishlist</a>
        <a class="dropdown-item" href="product-single.html">Single Product</a>
        <a class="dropdown-item" href="cart.html">Cart</a>
        <a class="dropdown-item" href="checkout.html">Checkout</a>
      </div> --}}
    </li>
      <li class="nav-item"><a href="/about" class="nav-link">Về chúng tôi</a></li>
{{--      <li class="nav-item"><a href="/blog" class="nav-link">Blog</a></li>--}}
      <li class="nav-item"><a href="/contact" class="nav-link">Liên hệ</a></li>
      <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"><span class="icon-shopping_cart"></span>[0]</a></li>

    </ul>
  </div>
</div>
</nav>
<!-- END nav -->
