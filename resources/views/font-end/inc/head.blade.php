<head>
    <title>Sen Ngoc Minh</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/fontend/images/icon-sen.jpg')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/jquery.timepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fontend/css/style.css')}}">
  </head>