@extends('font-end.master')


@section('content')
    <section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
            @foreach ($banners as $key => $banner)
                <div class="slider-item" style="background-image: url({{ asset($banner->path) }});">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

                            <div class="col-sm-12 ftco-animate text-center">
                                <h1 class="mb-2">Viên Ngọc Làng Sen</h1>
                                <h2 class="subheading mb-4">
                                    Tinh hoa trà Việt
                                </h2>
                                <p><a href="#" class="btn btn-danger">
                                        Đặt hàng ngay
                                    </a></p>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row no-gutters ftco-services">
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-shipped"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Vận chuyển toàn quốc</h3>
                            {{-- <span>On order over $100</span> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-diet"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Trà ngon Mood on</h3>
                            {{-- <span>Product well package</span> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-award"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Chất lượng cao</h3>
                            {{-- <span>Quality Products</span> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-customer-service"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Support 24/7</h3>
                            {{-- <span>24/7 Support</span> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-category ftco-no-pt">
        <div class="container">
            <div class="row" style="background-image: url('/assets/fontend/images/img/Sen Ngoc Minh-0022.jpg');">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 order-md-last align-items-stretch d-flex">
                            <div class="category-wrap-2 ftco-animate img align-self-stretch d-flex"
                                style="background-image: url(https://senngocminh.com/assets/fontend/images/img/Sen%20Ngoc%20Minh-0032.jpg);
							border-radius: 5%">
                                {{--							<div class="text text-center"> --}}
                                {{--								<h2>Vegetables</h2> --}}
                                {{--								<p>Protect the health of every home</p> --}}
                                {{--								<p><a href="#" class="btn btn-primary">Shop now</a></p> --}}
                                {{--							</div> --}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                                style="background-image: url(https://senngocminh.com/assets/fontend/images/img/Sen%20Ngoc%20Minh-0024.jpg);border-radius: 5%">
                                <!-- <div class="text px-3 py-1">
    {{--								<h2 class="mb-0"><a href="#">Fruits</a></h2> --}}
           </div> -->
                            </div>
                            <div class="category-wrap ftco-animate img d-flex align-items-end"
                                style="background-image: url(https://senngocminh.com/assets/fontend/images/img/Sen%20Ngoc%20Minh-0026.jpg);border-radius: 5%">
                                <!-- <div class="text px-3 py-1">
    {{--								<h2 class="mb-0"><a href="#">Vegetables</a></h2> --}}
           </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                        style="background-image: url(https://senngocminh.com/assets/fontend/images/img/Sen%20Ngoc%20Minh-0028.jpg);border-radius: 5%">
                        <!-- <div class="text px-3 py-1">
    {{--						<h2 class="mb-0"><a href="#">Juices</a></h2> --}}
         </div> -->
                    </div>
                    <div class="category-wrap ftco-animate img d-flex align-items-end"
                        style="background-image: url(https://senngocminh.com/assets/fontend/images/img/Sen%20Ngoc%20Minh-0030.jpg);border-radius: 5%">
                        <!-- <div class="text px-3 py-1">
    {{--						<h2 class="mb-0"><a href="#">Dried</a></h2> --}}
         </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    {{--				<span class="subheading">Featured Products</span> --}}
                    <h2 class="mb-4">Sản phẩm </h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach ($products as $key => $product)
                    <div class="col-md-6 col-lg-3 ftco-animate">

                        <div class="product">
                            <a href="/product/{{ $product->id }}" class="img-prod"><img class="img-fluid"
                                    src="{{ $images[$key]->path }}" alt="Colorlib Template" style="border-radius: 5%">
                                <!-- <span class="status">30%</span> -->
                                <div class="overlay" style="border-radius: 5%"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="/product/{{ $product->id }}">{{ $product->name }}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price">
                                            <!-- <span class="mr-2 price-dc">$120.00</span> -->
                                            <span class="price-sale">{{ number_format($product->price, 0) }} VND</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        <a href="#"
                                            class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                            <span><i class="ion-ios-menu"></i></span>
                                        </a>
                                        <a href="#"
                                            class="buy-now d-flex justify-content-center align-items-center mx-1">
                                            <span><i class="ion-ios-cart"></i></span>
                                        </a>
                                        <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                            <span><i class="ion-ios-heart"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <hr>





    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
{{--    <section class="ftco-section ftco-partner">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm ftco-animate">--}}
{{--                    <a href="#" class="partner" style="width: 200px;height: 200px"><img--}}
{{--                            src="{{ asset('assets/fontend/images/img/Sen Ngoc Minh-0044.jpg') }}" class="img-fluid"--}}
{{--                            alt="Colorlib Template"></a>--}}
{{--                </div>--}}
{{--                <div class="col-sm ftco-animate">--}}
{{--                    <a href="#" class="partner" style="width: 200px;height: 200px"><img--}}
{{--                            src="{{ asset('assets/fontend/images/img/Sen Ngoc Minh-0042.jpg') }}" class="img-fluid"--}}
{{--                            alt="Colorlib Template"></a>--}}
{{--                </div>--}}
{{--                <div class="col-sm ftco-animate">--}}
{{--                    <a href="#" class="partner" style="width: 200px;height: 200px"><img--}}
{{--                            src="{{ asset('assets/fontend/images/img/Sen Ngoc Minh-0040.jpg') }}" class="img-fluid"--}}
{{--                            alt="Colorlib Template"></a>--}}
{{--                </div>--}}
{{--                <div class="col-sm ftco-animate">--}}
{{--                    <a href="#" class="partner" style="width: 200px;height: 200px"><img--}}
{{--                            src="{{ asset('assets/fontend/images/img/Sen Ngoc Minh-0046.jpg') }}" class="img-fluid"--}}
{{--                            alt="Colorlib Template"></a>--}}
{{--                </div>--}}
{{--                <!-- <div class="col-sm ftco-animate">--}}
{{--        <a href="#" class="partner"><img src="images/partner-4.png" class="img-fluid"--}}
{{--          alt="Colorlib Template"></a>--}}
{{--       </div>--}}
{{--       <div class="col-sm ftco-animate">--}}
{{--        <a href="#" class="partner"><img src="images/partner-5.png" class="img-fluid"--}}
{{--          alt="Colorlib Template"></a>--}}
{{--       </div> -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
@endsection
