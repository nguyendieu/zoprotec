<!DOCTYPE html>
<html lang="en">
@include('font-end.inc.head')

<body class="goto-here">
    @include('font-end.inc.navbar')

    @yield('content')
    @include('font-end.inc.footer')

</body>

</html>
