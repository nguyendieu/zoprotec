@extends('admin.layouts.master')

@section('content')
    <main id="main" class="main">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"></h5>
                @include('admin.inc.alert')
            </div>


            <div class="card-body">
               <h3> Product : {{ $product->name }} </h3>
            </div>
            <table class="table table-bordered mb-3">
                <tr>
                    <th scope="col">
                        Stt
                    </th>
                    <th scope="col">
                        Image
                    </th>
                    <th scope="col">
                        Status
                    </th>
                    <th scope="col">
                        Created_ad
                    </th>
                    <th scope="col">
                        Action
                    </th>
                </tr>
                @foreach($product->images as $image)
                    <tr>
                        <td scope="col">{{ $image->id }}</td>
                        <td scope="col"><img src="{{ asset($image->path) }}" width="200" height="100"></td>
                        <td scope="col"><a href="#" class="btn btn-success">{{ $image->status == 1? 'Show' : 'Hide' }}</a></td>
                        <td scope="col">{{ $image->created_at }}</td>
                        <td scope="col">
                            <form action="/admin/products/{{$product->id}}/images/delete/{{$image->id}}" method="post" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger"><i class="ri-chat-delete-line"></i></button>
                            </form>
                        </td>
                    </tr>

                @endforeach
            </table>
            <div>
                <a href="{{route('list.products')}}" class="btn btn-info">List product</a>
            </div>
        </div>

    </main>
@endsection
