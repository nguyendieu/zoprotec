<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link collapsed" href="index.html">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <li class="nav-item">
            <a class="nav-link  collapsed"  href="{{ route('list.products') }}">
                <i class="bi bi-menu-button-wide"></i><span>Products</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link collapsed"  href="{{ route('list.banners') }}">
                <i class="bi bi-menu-button-wide"></i><span>Banners</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link collapsed"  href="{{ route('list.posts') }}">
                <i class="bi bi-menu-button-wide"></i><span>Posts</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link collapsed"  href="{{ route('list.contacts') }}">
                <i class="bi bi-menu-button-wide"></i><span>Contacts</span>
            </a>
        </li>
        <li class="nav-heading">Pages</li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="users-profile.html">
                <i class="bi bi-person"></i>
                <span>Profile</span>
            </a>
        </li><!-- End Profile Page Nav -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="pages-faq.html">
                <i class="bi bi-question-circle"></i>
                <span>F.A.Q</span>
            </a>
        </li>

    </ul>
</aside>
