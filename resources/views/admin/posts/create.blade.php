@extends('admin.layouts.master')

@section('content')
    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Form create add new Post</h5>

                            <!-- General Form Elements -->
                            <form class="mb-3" method="POST" action="">
                                @csrf
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Tiêu đề</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Mô tả</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="description"
                                                  style="height: 100px"></textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <legend class="col-form-label col-sm-2 pt-0">Trạng thái</legend>
                                    <div class="col-sm-10">

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="status" id="gridCheck1">
                                            <label class="form-check-label" for="gridCheck1">
                                                Status
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label" >Nội dung</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="content" style="height: 100px" id="editor"> </textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label" >Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="image">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form><!-- End General Form Elements -->
                            @include('admin.inc.alert')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor');
    </script>
@endsection
