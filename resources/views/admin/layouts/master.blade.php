<!DOCTYPE html>
<html lang="en">

@include('admin.inc.head')

<body>

@include('admin.inc.navbar')

<!-- ======= Sidebar ======= -->
@include('admin.inc.sidebar')
<!-- ==== Content ====-->
@yield('content')

@include('admin.inc.footer')
</body>

</html>
