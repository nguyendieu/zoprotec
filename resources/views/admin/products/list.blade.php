@extends('admin.layouts.master')

@section('content')
    <main id="main" class="main">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">List Products</h5>
                @include('admin.inc.alert')
                <a href="{{ route('create.product') }}" class="btn btn-info mb-2">New Product</a>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Price</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">Active</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{ $product->id }}</th>
                            <td><a href="{{ route('list.images.product',$product->id) }}">{{$product->name}}</a></td>
                            <td>{{$product->status == 1 ? 'Show' : 'Hide'}}</td>
                            <td>{{number_format($product->price, 0) }} đ</td>
                            <td>{{$product->created_at}}</td>
                            <td>
                                <a href="/admin/products/{{ $product->id }}/add-image" class="btn btn-info">Add image
                                </a>
                                <a href="/admin/products/edit/{{$product->id}}" class="btn btn-success"><i
                                        class="ri-edit-box-line"></i></a>
                                <form action="{{route('delete.product',$product->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                <button class="btn btn-danger"><i class="ri-chat-delete-line"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </main>
@endsection
