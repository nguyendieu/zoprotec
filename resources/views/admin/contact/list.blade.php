@extends('admin.layouts.master')

@section('content')
    <main id="main" class="main">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">List Contact</h5>
                @include('admin.inc.alert')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Name</th>
                        <th scope="col">Mes</th>
                        <th scope="col">Start Date</th>

                    </tr>
                    </thead>
                    <tbody>
                   @foreach($contacts as $contact)
{{--                       @dd($contact)--}}
                        <tr>
                            <th scope="row">{{$contact->id}}</th>
                            <td>{{ $contact->name }}<a href=""></a></td>
                            <td>{{ $contact->mes }}</td>
                            <td> {{ $contact->created_at }}</td>

                            <td>
                                <form action="{{ route('contact.delete', $contact->id) }}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                <button class="btn btn-danger">
{{--                                    <img src="{{asset('assets/admin/img/delete.png')}}">--}}
                                    <i class="ri-chat-delete-line"></i>
                                </button>
                                </form>
                            </td>
                        </tr>
                   @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </main>
@endsection
