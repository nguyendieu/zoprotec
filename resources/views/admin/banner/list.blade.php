@extends('admin.layouts.master')

@section('content')
    <main id="main" class="main">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">List Banners</h5>
                @include('admin.inc.alert')
                <a href="{{route('create.banners')}}" class="btn btn-info mb-2">New Banner</a>
                <table class="table table-hover px-0">
                    <thead>
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Banner</th>
                        <th scope="col">Status</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banners as $key=>$banner)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $banner->title }}</td>
                            <td>
                                <form action="{{route('change.status.banner',$banner->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <button type="submit" class="btn btn-info text-success">
                                        {{ $banner->status == 1 ? 'Show' : 'Hide'}}
                                    </button>
                                </form>
                            </td>
                            <td><img src="{{ asset($banner->path) }}" width='300' height="200"></td>
                            <td >
                                <a href="./admin/banners/{{ $banner->id }}" class="btn btn-info"><i
                                        class="bi bi-info-square"></i></a>
                                <a href="./admin/banners/edit/{{$banner->id}}" class="btn btn-success"><i
                                        class="ri-edit-box-line"></i></a>
                                <form action="{{route('delete.banner',$banner->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger"><i class="ri-chat-delete-line"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </main>
@endsection
