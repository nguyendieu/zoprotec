<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'content' => 'required',
            'price' =>'required'
        ];
    }
    public function messages():array
    {
        return [
            'name.required' => 'Name required',
            'description.required' => 'Description required',
            'content.required' => 'Content required',
            'price.required' => 'Content required',
        ];
    }
}
