<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'=>'required|max:255',
            'description' => 'required',
            'content' => 'required',
            'image' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Bạn phải nhập tiêu đề',
            'description.required' => 'Bạn phải nhập mô tả',
            'content.required' => 'Bạn phải nhập nội dung bài viết',
            'image.required' => 'Bạn phải chọn ảnh cho bài viết',
            'title.max' => 'Tiêu đề không được dài quá 255 kí tự',
            // 'image.mimes' => 'Ảnh không đúng định dạng'
        ];
    }
}
