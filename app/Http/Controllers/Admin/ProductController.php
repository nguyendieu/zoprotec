<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductFormRequest;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\NoReturn;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $products = $this->productRepository->all();
        return view('admin.products.list', [
            'title' => 'Products',
            'products' => $products
        ]);
    }

    public function create()
    {
        return view('admin.products.create', [
            'title' => 'Create New Product'
        ]);
    }

    public function store(ProductFormRequest $request)
    {
        $request->input('status') ? $status = 1 : $status = 0;
        $product = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status' => $status,
            'content' => $request->input('content'),
            'price' => $request->input('price')
        ];
//        dd($product);
        if ($this->productRepository->create($product)) {
            return redirect()->route('list.products')->with('success', 'Create new product successfully');
        }
        return redirect()->back()->with('errors', 'Create new product fails');
    }
    public function show($id){
        $product = $this->productRepository->find($id);
        //dd($product);
    }
    public function edit($id){
        $product = $this->productRepository->find($id);
        //dd($product);
        return view('admin.products.edit',[
            'title' => 'Edit product',
            'product' => $product
        ]);
    }
    public function update(ProductFormRequest $request,$id){
        $request->input('status') ? $status = 1 : $status = 0;
        $product = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status' => $status,
            'content' => $request->input('content'),
            'price'=>$request->input('price')
        ];
        if ($this->productRepository->update($product,$id)){
            return redirect()->route('list.products')->with('success','Update product successfully');
        }
        return redirect()->route('list.products')->with('error','Update product fails');
    }
    public function destroy($id){
        dd($id);
    }
}
