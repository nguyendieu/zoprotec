<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\BannerFormRequest;
use App\Repositories\Banner\BannerRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    protected $bannerRepository;

    public function __construct(BannerRepositoryInterface $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->bannerRepository->all();
        //dd($banner);
        return view('admin.banner.list', [
            'title' => 'List Banners',
            'banners' => $banners
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.create', [
            'title' => 'Form create new Banner'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerFormRequest $request)
    {
        $request->input('status') ? $status = 1 : $status = 0;
        if ($request->hasFile('path')) {
            $file = $request->file('path');
            $ext = 'banner-' . time() . '.' . $file->getClientOriginalExtension();
            $fileName = 'uploads/' . $ext;
            $path = $file->move('uploads', $ext);
        }
        $banner = [
            'title' => $request->input('title'),
            'path' => $fileName,
            'status' => $status
        ];
        if ($this->bannerRepository->create($banner)) {
            return redirect()->route('list.banners')->with('success', 'Create banner successfully');
        }
        return redirect()->route('list.banners')->with('error', 'Create banner fails');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = $this->bannerRepository->find($id);
        return view('admin.banner.edit', [
            'banner' => $banner,
            'title' => 'Form edit Banner'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = $this->bannerRepository->find($id);
        if ($banner) {
            file::delete($banner->path);
            $this->bannerRepository->delete($id);
            return redirect()->route('list.banners')->with('success', 'Delete banner successfully');
        }
        return redirect()->route('list.banners')->with('error', 'Delete banner fails');
    }

    public function changeStatus($id)
    {
        $banner = $this->bannerRepository->find($id);
        $banner->status == 1 ? $status = 0 : $status = 1;
//        dd($status);
        $newBanner = [
            'title' => $banner->title,
            'path'=> $banner->path,
            'status' => $status
        ];
//        $banner->status = $status;
        if($this->bannerRepository->update($newBanner ,$id)) {
            return redirect()->route('list.banners')->with('success','change success');
        }
        return redirect()->route('list.banners')->with('fails','change fails');
    }
}
