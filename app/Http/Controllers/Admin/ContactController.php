<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactRepositoryInterface;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected $contactRepository;
    public function __construct(ContactRepositoryInterface $contactRepository) {
        $this->contactRepository = $contactRepository;
    }
    public function index(){
        $contacts = $this->contactRepository->all();
        return view('admin.contact.list', [
            'title' => 'List Banners',
            'contacts' => $contacts
        ]);
    }
    public function destroy($id)
    {
        $contact = $this->contactRepository->find($id);
        if (isset($contact)){
            $this->contactRepository->delete($id);
            return redirect()->route('list.contacts')->with('success', 'Delete banner successfully');
        }
        else{
            return redirect()->route('list.contacts')->with('errors', 'Delete banner errors');
        }
    }
}
