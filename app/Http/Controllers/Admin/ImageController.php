<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Image\ImageRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    protected $imageRepository;
    protected $productRepository;

    public function __construct(ImageRepositoryInterface   $imageRepository,
                                ProductRepositoryInterface $productRepository
    )
    {
        $this->imageRepository = $imageRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idProduct)
    {
        $product = $this->productRepository->getImages($idProduct);

        //  dd($product->images);

//        $product = $this->productRepository->find($idProduct);
//        $images = $this->imageRepository->showImage($idProduct);
        return view('admin.image.list', [
            'title' => 'List image',
            'product' => $product,
//            'images' => $images
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($product)
    {
        return view('admin.image.create', [
            'title' => 'Add image product'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $product)
    {
//        dd($request->all());
        if ($request->input('status')) {
            $status = 1;
        } else {
            $status = 0;
        }
        if ($request->hasFile('path')) {
            $file = $request->file('path');
            $ext = 'banner-' . time() . '.' . $file->getClientOriginalExtension();
            $fileName = 'uploads/' . $ext;
            $path = $file->move('uploads', $ext);
        }       
        $image = [
            'id_product' => $product,
            'path' => $fileName,
            'status' => $status
        ];
        if ($this->imageRepository->create($image)) {
            return redirect()->route('list.images.product',$product)->with('success', 'add image successfully');
        }
        return redirect()->route('list.images.product',$product)->with('error', 'add image fails');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idProduct,$idImage)
    {
            $image = $this->imageRepository->find($idImage);
            if ($image) {
                $this->imageRepository->delete($idImage);
                return redirect()->route('list.images.product',$idProduct)->with('success','delete image product successfully');
            }
        return redirect()->route('list.images.product',$idProduct)->with('error','delete image product fails');
    }
}
