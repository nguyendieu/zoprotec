<?php

namespace App\Http\Controllers\FontEnd;

use App\Http\Controllers\Controller;
use App\Repositories\Image\ImageRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductSigleController extends Controller
{
    protected $productRepository;
    protected $imageRepository;

    public function __construct(ProductRepositoryInterface $productRepository,ImageRepositoryInterface $imageRepository)
    {
        $this->productRepository = $productRepository;
        $this->imageRepository = $imageRepository;
    }
    public function show($id){
        $product = $this->productRepository->find($id);
        $images = $this->imageRepository->showProductFirst($id);
        $productAll = $this->productRepository->all();
        foreach ($productAll as $value) {
            $imgs[] = $this->imageRepository->showProductFirst($value->id);
        }
        // dd($images);
        // dd($product);
        // dd($imgs);
        return view('font-end.product',[
            "product"=>$product,
            "images" => $images,
            "productAll" => $productAll,
            "imgs" => $imgs
        ]);
    }
}
