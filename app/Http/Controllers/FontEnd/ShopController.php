<?php

namespace App\Http\Controllers\FontEnd;

use App\Http\Controllers\Controller;
use App\Repositories\Image\ImageRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    protected $productRepositoryIinterface;
    protected $imageRepositoryInterface;
    public function __construct(ProductRepositoryInterface $productRepositoryIinterface,ImageRepositoryInterface $imageRepositoryInterface)
    {
        $this->imageRepositoryInterface = $imageRepositoryInterface;
        $this->productRepositoryIinterface = $productRepositoryIinterface;
    }
    public function index(){
       $products =  $this->productRepositoryIinterface->all();
       foreach($products as $product){
        $images[] = $this->imageRepositoryInterface->showProductFirst(
            $product->id
           );
       }
    //   dd($images);
        return view('font-end.shop',[
            'products'=> $products,
            'images' => $images
        ]);
    }
}
