<?php

namespace App\Http\Controllers\FontEnd;

use App\Http\Controllers\Controller;
use App\Repositories\Banner\BannerRepository;
use App\Repositories\Banner\BannerRepositoryInterface;
use App\Repositories\Image\ImageRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Services\ProductService;

class HomeController extends Controller
{
    protected $bannerRepository;
    protected $productRepository;
    protected $imageRepository;
    protected $productService;

    public function __construct(BannerRepositoryInterface $bannerRepository,
        ProductRepositoryInterface $productRepository,
        ImageRepositoryInterface $imageRepository,
        ProductService $productService
        ) {
        $this->bannerRepository = $bannerRepository;
        $this->productRepository = $productRepository;
        $this->imageRepository = $imageRepository;
        $this->productService = $productService;
    }
    public function index()
    {
        $banners = $this->bannerRepository->all();
        $products = $this->productRepository->all();
        $images = $this->imageRepository->all();
        
        // dd($products->image->path);
        foreach($products as  $product){
            $images[] = $this->productService->showImage($product->id);
        }
    //    dd($images);
        return view('font-end.home', [
            'products'=>$products,
            'banners' => $banners,
            'images' => $images
        ]);
    }
}
