<?php

namespace App\Repositories\Banner;

use App\Models\Banner;
use App\Repositories\BasicRepository;

class BannerRepository extends BasicRepository implements BannerRepositoryInterface
{

    public function getModel()
    {
        return Banner::class;
    }
}
