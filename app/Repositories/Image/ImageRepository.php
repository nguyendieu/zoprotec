<?php

namespace App\Repositories\Image;

use App\Models\Image;
use App\Repositories\BasicRepository;

class ImageRepository extends BasicRepository implements ImageRepositoryInterface
{

    public function getModel()
    {
        return Image::class;
    }

    public function showImage($product)
    {
        return $this->model->where('id_product',$product)->get();
    }
    public function showProduct($idProduct){
        return $this->model->with('product')->where('id_product',$idProduct)->get();
    }
    public function showProductFirst($idProduct){
        return $this->model->with('product')->where('id_product',$idProduct)->first();
    }
}
