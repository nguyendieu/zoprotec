<?php

namespace App\Repositories\Image;

use App\Repositories\RepositoryInterface;

interface ImageRepositoryInterface extends RepositoryInterface
{
    public function showImage($product);
    public function showProduct($product);
    public function showProductFirst($product);
}
