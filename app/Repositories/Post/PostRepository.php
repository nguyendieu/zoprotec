<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\BasicRepository;

class PostRepository extends BasicRepository implements PostRepositoryInterface
{

    public function getModel()
    {
        return Post::class;
    }
}
