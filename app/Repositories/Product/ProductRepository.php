<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BasicRepository;
use App\Models\Image;

class ProductRepository extends BasicRepository implements ProductRepositoryInterface
{

    public function getModel()
    {
        return Product::class;
    }

    public function getImages($product)
    {
        return $this->model->with('images')->where('id',$product)->first();
    }
}
