<?php

namespace App\Repositories\Contact;

use App\Models\Contact;
use App\Repositories\BasicRepository;

class ContactRepository extends BasicRepository implements ContactRepositoryInterface
{

    public function getModel()
    {
        return Contact::class;
    }
}
