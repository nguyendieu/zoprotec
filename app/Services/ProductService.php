<?php

namespace App\Services;

use App\Repositories\Image\ImageRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;

class ProductService
{
    public $productRepository;
    public $imageRepository;
    public function __construct(ProductRepositoryInterface $productRepository,ImageRepositoryInterface $imageRepository)
    {
        $this->productRepository = $productRepository;
        $this->imageRepository = $imageRepository;
    }
    public function showImage($idProduct){
        return $this->imageRepository->showImage($idProduct);
    }
}
