<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'price',
        'content',
        'status'
    ];

    public function images()
    {
        return $this->hasMany(Image::class,'id_product','id');
    }
}
