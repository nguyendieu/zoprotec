<?php

namespace App\Providers;

use App\Repositories\Banner\BannerRepository;
use App\Repositories\Banner\BannerRepositoryInterface;
use App\Repositories\BasicRepository;
use App\Repositories\Contact\ContactRepository;
use App\Repositories\Contact\ContactRepositoryInterface;
use App\Repositories\Image\ImageRepository;
use App\Repositories\Image\ImageRepositoryInterface;
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\RepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class,BasicRepository::class);
        #product
        $this->app->bind(ProductRepositoryInterface::class,ProductRepository::class);
        #banner
        $this->app->bind(BannerRepositoryInterface::class,BannerRepository::class);
        #user
        $this->app->bind(UserRepositoryInterface::class,UserRepository::class);
        #image
        $this->app->bind(ImageRepositoryInterface::class,ImageRepository::class);
        #posts
        $this->app->bind(PostRepositoryInterface::class,PostRepository::class);
        #contact
        $this->app->bind(ContactRepositoryInterface::class, ContactRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
